import React from 'react';
import ReactDOM from 'react-dom';
import RootQuiz from './Quiz';
import './component/quizfolder/App.css'

ReactDOM.render(
  <React.StrictMode>
    <RootQuiz />
  </React.StrictMode>,
  document.getElementById('root')
);
