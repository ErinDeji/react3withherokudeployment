import React, { Component } from 'react';


class FlovorComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: "grape fruit"
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(e) {
        this.setState({
            value: e.target.value
        })
    }

    handleSubmit(e) {
        alert(this.state.value.toUpperCase())
        e.preventDefault()
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Pick your favorite flavor
                     <select onChange={this.handleChange}>
                        <option value="grapefruit">grape fruit</option>
                        <option value="applefruit">apple fruit</option>
                        <option value="orangefruit">orange fruit</option>
                        <option value="mangofruit">mango fruit</option>
                        <option value="bananafruit">banana fruit</option>
                    </select>
                </label>
                <input type="submit" value="Submit" />
            </form>
        );
    }
}

export default FlovorComponent