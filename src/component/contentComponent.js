import React, { Component } from 'react';
import InputComponent from './inputComponent';
import EssayComponent from './essayComponent';
import FlovorComponent from './flavourForm';
import ReservationComponent from './reservationComponent';


class Content extends Component {
    constructor(props) {
        super(props)

        this.state = {
            myInputValue: "My Input"
        }

        this.myInputChanged = this.myInputChanged.bind(this)
    }

    myInputChanged(e) {
        var itemValue = e.target.value

        this.setState({
            myInputValue: itemValue
        })
    }

    render() {
        return (
            <div className="App-intro">
                <h1>Components Life Cycle!</h1>
                <InputComponent inputValue={this.state.myInputValue}
                    change={this.myInputChanged}
                />
                <h4>{this.state.myInputValue}</h4>
                <EssayComponent />
                <FlovorComponent />
                <hr />
                <ReservationComponent />
            </div>
        );
    }
}

export default Content