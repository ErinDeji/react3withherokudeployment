import React, { Component } from 'react';


class ReservationComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isGoing: true,
            numberOfGuests: 2
        }
        this.handleChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleInputChange(e) {
        const target = e.target
        const value = target.type === "checkbox" ? target.checked : target.value
        const name = target.name
        console.log(name, value);
        this.setState({
            [name]: value
        })
    }

    handleSubmit(e) {
        alert(`Going: ${this.state.isGoing}, Guests: ${this.state.numberOfGuests}`)
        e.preventDefault()
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Are you going for the party ?:
                     <input
                        name="isGoing"
                        type="checkbox"
                        checked={this.state.isGoing}
                        onChange={this.handleInputChange} />
                </label>
                <hr />
                <label>
                    How many guest will you bring ?:
                     <input
                        name="isGoing"
                        type="number"
                        checked={this.state.numberOfGuests}
                        onChange={this.handleInputChange} />
                </label>
                <input type="submit" value="submit" />
            </form>
        );
    }
}

export default ReservationComponent