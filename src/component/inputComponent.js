import React, { Component } from 'react';

class InputComponent extends Component {
    render() {
        return (
            <div>
                <h5>My input Component</h5>
                <input value={this.props.inputValue}
                    onChange={this.props.change}
                />
            </div>
        );
    }
}

export default InputComponent