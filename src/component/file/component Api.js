import React, { Component } from 'react';
import ReactDOM from 'react-dom'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            headerText: "Welcome to oggz website",
            contentText: "A basic content bitch"
        }
    }

    render() {
        return (
            <div className="container">
                {
                    /* <h1>{this.state.headerText}</h1>
                    <p>{this.state.contentText}</p> */
                }
                <Header head={this.state.headerText} />
                <Content />
            </div>
        )
    }
}


class Header extends Component {
    render() {
        return (
            <header className="header"><h1
                style={{ color: "red" }}>
                {this.props.head}
            </h1></header>
        )
    }
}

class Content extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: ["hey"],
            count: 0
        }
        this.updateMyState = this.updateMyState.bind(this)
        this.forceUpdateRandomNumber = this.forceUpdateRandomNumber.bind(this)
        this.findMyDOMNode = this.findMyDOMNode.bind(this)

    }
    // when you try to use a function, you need to bind it to the particular class
    updateMyState() {
        var count = this.state.count
        count++
        var item = " Click -" + count
        var myArray = this.state.data
        myArray.push(item)
        this.setState({
            data: myArray, count
        })
    }

    forceUpdateRandomNumber() {
        this.forceUpdate()
    }

    findMyDOMNode() {
        var myDiv = document.getElementById('myDiv')
        ReactDOM.findDOMNode(myDiv).style.color = 'red'
    }

    render() {
        return (
            <div>
                <h1>Content Api</h1>
                <button onClick={this.updateMyState}>press</button>
                <h4>State data: {this.state.data}</h4>
                <button onClick={this.forceUpdateRandomNumber}>Random Number</button>
                <h4>Random Number: {Math.random()}</h4>
                <button onClick={this.findMyDOMNode}>Find my dom node</button>
                <h4 id="myDiv">This is my div</h4>
            </div>
        )
    }
}

export default App