import React, { Component } from 'react';
import PropTypes from 'prop-types'

export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            headerText: "Welcome to oggz website",
            contentText: "A basic content bitch"
        }
    }
    render() {
        return (<div className="container">
            {
                /* <h1>{this.state.headerText}</h1>
                <p>{this.state.contentText}</p> */
            }
            <Header head={this.state.headerText} />
            <Content content={this.state.contentText} />
        </div>
        )
    }
}


class Header extends Component {
    render() {
        return (
            <header className="header"><h1
                style={{ color: "red" }}>
                {this.props.head}
            </h1></header>
        )
    }
}

class Content extends Component {
    render() {
        return (
            <div>
                <div>
                    <h2>Array: {this.props.propArray}</h2>
                    <h2>Bool: {this.props.propBool ? "true" : "false"}</h2>
                    <h2>Function: {this.props.propFunc(5)}</h2>
                    <h2>Number: {this.props.propNumber}</h2>
                    <h2>String: {this.props.propString}</h2>
                    <h2>Object: {this.props.propObject.objectName1}</h2>
                    <h2>Object: {this.props.propObject.objectName2}</h2>
                    <h2>Object: {this.props.propObject.objectName3}</h2>
                </div>
            </div>
        )
    }
}


Component.propTypes = {
    propArray: PropTypes.array.isRequired,
    propBool: PropTypes.bool.isRequired,
    propFunc: PropTypes.func,
    propNumber: PropTypes.number,
    propString: PropTypes.string,
    propObject: PropTypes.object
}

Component.defaultProps = {
    propArray: [1, 2, 3, 4, 5],
    propBool: true,
    propFunc: function (e) { return e },
    propNumber: 1,
    propString: "String Value .....",
    propObject: {
        objectName1: "objectValue1",
        objectName2: "objectValue2",
        objectName3: "objectValue3"
    }
}
