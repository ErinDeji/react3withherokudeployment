import React, { Component } from 'react';
export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      headerText: "Welcome to oggz website",
      contentText: "A basic content bitch"
    }
  }
  render() {
    return (<div className="container">
      {
        /* <h1>{this.state.headerText}</h1>
        <p>{this.state.contentText}</p> */
      }
      <Header head={this.state.headerText} />
      <Content content={this.state.content} />
      <ClockComponent />
    </div>
    )
  }
}

class ClockComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      date: new Date()
    }
  }

  componentDidMount() {
    this.timeId = setInterval(() => {
      this.tick()
    }, 1000)
  }

  tick() {
    this.setState({ date: new Date() })
  }

  componentWillUnmount() {
    clearInterval(this.timeId)
  }

  render() {
    return (
      <div>

        <h2>The current time is: {
          this.state.date.toLocaleTimeString()
        }</h2>
      </div>)
  }
}

class Header extends Component {
  render() {
    return (
      <header className="header"><h1
        style={{ color: "red" }}>
        {this.props.head}
      </h1></header>

    )
  }
}


class TableRow extends Component {
  render() {
    return (
      <tr>
        {this.props.data.id}
        {this.props.data.name}
        {this.props.data.age}
      </tr>
    )
  }
}
class Content extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [
        {
          "id": 1,
          "name": "Foo",
          "age": "30"
        },
        {
          "id": 2,
          "name": "Bar",
          "age": "20"
        },
        {
          "id": 3,
          "name": "Baz",
          "age": "16"
        }
      ]
    }
  }
  render() {
    return (
      <div>{this.props.content}

        <table width="50">
          <thead> <th>
            <td>Id</td>
            <td>Name</td>
            <td>Age</td>
          </th>
          </thead>
          <tbody>{
            this.state.data.map((person, i) =>
              <TableRow key={i} data={person} />)
          }
          </tbody>
        </table>
      </div>
    )
  }
}