import React, { Component } from 'react';


class EssayComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: "Once upon a time"
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(e) {
        this.setState({
            value: e.target.value
        })
    }

    handleSubmit(e) {
        alert(this.state.value)
        e.preventDefault()
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <textarea value={this.state.value}
                    onChange={this.handleChange} />
                <h4>{this.state.value}</h4>
                <button type="submit">Submit</button>
            </form>
        );
    }
}

export default EssayComponent