import React, { Component } from 'react';
// import Content from './component/contentComponent';
import {
    NavLink
} from 'react-router-dom'


class Blog extends Component {

    render() {
        return (
            <div><h2>
                Blog Page
            </h2>
                <ul>
                    <NavLink activeStyle={{ color: 'teal' }} to='/blog/123' className='nav'> Blog 1 </NavLink>
                    <NavLink activeStyle={{ color: 'teal' }} to='/blog/345' className='nav'> Blog 2 </NavLink>
                    <NavLink activeStyle={{ color: 'teal' }} to='/blog/678' className='nav'> Blog 3 </NavLink>
                </ul>
            </div>
        );
    }
}


export default Blog;