import React, { Component } from 'react';
import {
  NavLink
} from 'react-router-dom'
// import Content from './component/contentComponent';


class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      headerText: "Welcome to React!",
      contentText: "In this lecture, we will go over the Components Life Cycle methods"
    };
  }
  render() {
    return (
      <div className="App">
        <ul>
          <NavLink activeStyle={{ color: 'teal' }} to='/home' className='nav'> Home </NavLink>
          <NavLink activeStyle={{ color: 'teal' }} to='/about' className='nav'> About </NavLink>
          <NavLink activeStyle={{ color: 'teal' }} to='/contact' className='nav'> Contact </NavLink>
          <NavLink activeStyle={{ color: 'teal' }} to='/blog' className='nav'> Blog </NavLink>
        </ul>
      </div>
    );
  }
}


export default App;
